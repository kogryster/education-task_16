# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: EKATERINA GERASIMOVA

**E-MAIL**: kogryster@gmail.com

# SOFTWARE

- JDK 1.8
- MS WINDOWS 7 / MS WINDOWS 10

# PROGRAM BUILD

```bash
mvn clean instal
```

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOT

**JSE-16**: https://yadi.sk/d/5oRx2SnSYxzGOA?w=1