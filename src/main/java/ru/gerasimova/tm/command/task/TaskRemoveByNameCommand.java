package ru.gerasimova.tm.command.task;

import ru.gerasimova.tm.entity.Task;
import ru.gerasimova.tm.util.TerminalUtil;

public final class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().removeOneByName(userId, name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
