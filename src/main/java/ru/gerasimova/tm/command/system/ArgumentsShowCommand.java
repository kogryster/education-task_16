package ru.gerasimova.tm.command.system;

import ru.gerasimova.tm.command.AbstractCommand;

import java.util.Arrays;

public final class ArgumentsShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        final String[] arguments = serviceLocator.getCommandService().getArguments();
        System.out.println(Arrays.toString(arguments));
    }

}
