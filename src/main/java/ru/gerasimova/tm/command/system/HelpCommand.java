package ru.gerasimova.tm.command.system;

import ru.gerasimova.tm.command.AbstractCommand;
import ru.gerasimova.tm.dto.Command;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String description() {
        return "Display terminal commands";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        System.out.println("[OK]");
        System.out.println("[HELP:]");
        final Command[] commands = serviceLocator.getCommandService().getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

}
