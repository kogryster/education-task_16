package ru.gerasimova.tm.command.project;

import ru.gerasimova.tm.entity.Project;
import ru.gerasimova.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Override
    public String description() {
        return "Remove project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().removeOneByIndex(userId, index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
