package ru.gerasimova.tm.repository;

import ru.gerasimova.tm.api.repository.ITaskRepository;
import ru.gerasimova.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, final Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (userId.equals(task.getUserId()))
            tasks.remove(task);
        else return;
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        final List<Task> tasks = findAll(userId);
        this.tasks.removeAll(tasks);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        for (final Task task : tasks) {
            boolean idMatch = false;
            boolean userIdMatch = false;
            if (id.equals(task.getId())) idMatch = true;
            if (userId.equals(task.getUserId())) userIdMatch = true;
            if (idMatch && userIdMatch) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        if (!userId.equals(task.getUserId())) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        for (final Task task : tasks) {
            boolean nameMatch = false;
            boolean userIdMatch = false;
            if (name.equals(task.getName())) nameMatch = true;
            if (userId.equals(task.getUserId())) userIdMatch = true;
            if (nameMatch && userIdMatch) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        if (!userId.equals(task.getUserId())) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        if (!userId.equals(task.getUserId())) return null;
        remove(userId, task);
        return task;
    }

}
