package ru.gerasimova.tm.repository;

import ru.gerasimova.tm.api.repository.ICommandRepository;
import ru.gerasimova.tm.constant.ArgumentConst;
import ru.gerasimova.tm.constant.TerminalConst;
import ru.gerasimova.tm.dto.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "Display terminal commands."
    );

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Show developer info."
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null, "Close application."
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, "Display information about system."
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Show version info."
    );

    public static final Command ARGUMENT = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show program arguments."
    );

    public static final Command COMMAND = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Show program commands."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "Create new task."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "Remove all tasks."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "Show task list."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "Create new project."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Remove all projects."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "Show project list."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null, "Update task by index."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null, "Update task by id."
    );

    private static final Command TASK_VIEW_BY_ID = new Command(
            TerminalConst.TASK_VIEW_BY_ID, null, "Show task by id."
    );

    private static final Command TASK_VIEW_BY_NAME = new Command(
            TerminalConst.TASK_VIEW_BY_NAME, null, "Show task by name."
    );

    private static final Command TASK_VIEW_BY_INDEX = new Command(
            TerminalConst.TASK_VIEW_BY_INDEX, null, "Show task by index."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null, "Remove task by id."
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TASK_REMOVE_BY_NAME, null, "Remove task by name."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null, "Remove task by index."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null, "Update project by index."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null, "Update project by id."
    );

    private static final Command PROJECT_VIEW_BY_ID = new Command(
            TerminalConst.PROJECT_VIEW_BY_ID, null, "Show project by id."
    );

    private static final Command PROJECT_VIEW_BY_NAME = new Command(
            TerminalConst.PROJECT_VIEW_BY_NAME, null, "Show project by name."
    );

    private static final Command PROJECT_VIEW_BY_INDEX = new Command(
            TerminalConst.PROJECT_VIEW_BY_INDEX, null, "Show project by index."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null, "Remove project by id."
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.PROJECT_REMOVE_BY_NAME, null, "Remove project by name."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null, "Remove task by index."
    );

    private static final Command LOGIN = new Command(
            TerminalConst.LOGIN, null, "Login user in program."
    );

    private static final Command LOGOUT = new Command(
            TerminalConst.LOGOUT, null, "Logout user in program."
    );

    private static final Command REGISTRY = new Command(
            TerminalConst.REGISTRY, null, "Registry new user."
    );

    private static final Command USER_INFO = new Command(
            TerminalConst.USER_INFO, null, "Info about current user."
    );

    private static final Command USER_PASSWORD_UPDATE = new Command(
            TerminalConst.USER_PASSWORD_UPDATE, null, "Update password for current user."
    );

    private static final Command USER_LOCK = new Command(
            TerminalConst.USER_LOCK, null, "Lock user."
    );

    private static final Command USER_UNLOCK = new Command(
            TerminalConst.USER_UNLOCK, null, "Unlock user."
    );

    private static final Command USER_REMOVE_BY_ID = new Command(
            TerminalConst.USER_REMOVE_BY_ID, null, "Remove user by login."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, ABOUT, VERSION, INFO, COMMAND, ARGUMENT,
            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID,
            TASK_VIEW_BY_ID, TASK_VIEW_BY_NAME,
            TASK_VIEW_BY_INDEX, TASK_REMOVE_BY_ID,
            TASK_REMOVE_BY_NAME, TASK_REMOVE_BY_INDEX,
            PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID,
            PROJECT_VIEW_BY_ID, PROJECT_VIEW_BY_NAME,
            PROJECT_VIEW_BY_INDEX, PROJECT_REMOVE_BY_ID,
            PROJECT_REMOVE_BY_NAME, PROJECT_REMOVE_BY_INDEX,
            LOGIN, LOGOUT, REGISTRY,
            USER_INFO, USER_PASSWORD_UPDATE,
            USER_LOCK, USER_UNLOCK, USER_REMOVE_BY_ID,
            EXIT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }

        return Arrays.copyOfRange(result, 0, index);
    }

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArguments() {
        return ARGS;
    }

}
