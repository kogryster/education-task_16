package ru.gerasimova.tm.constant;

public interface ArgumentConst {

    String HELP = "-h";

    String VERSION = "-v";

    String ABOUT = "-a";

    String INFO = "-i";

    String COMMANDS = "-cmd";

    String ARGUMENTS = "-arg";

}
