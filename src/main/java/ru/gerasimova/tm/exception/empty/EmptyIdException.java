package ru.gerasimova.tm.exception.empty;

public class EmptyIdException extends AbstractEmptyException {

    public EmptyIdException() {
        super("Error! Id is empty...");
    }

}
