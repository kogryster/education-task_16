package ru.gerasimova.tm.exception.empty;

public class EmptyLoginException extends AbstractEmptyException {

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }
}
