package ru.gerasimova.tm.api.repository;

import ru.gerasimova.tm.dto.Command;

public interface ICommandRepository {

    String[] getCommands(Command... values);

    String[] getArgs(Command... values);

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArguments();

}
