package ru.gerasimova.tm.api.service;

import ru.gerasimova.tm.entity.User;
import ru.gerasimova.tm.enumeration.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    User updateUserPassword(String id, String password);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);

}
