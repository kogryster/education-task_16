package ru.gerasimova.tm.api.service;

import ru.gerasimova.tm.entity.User;
import ru.gerasimova.tm.enumeration.Role;

public interface IAuthService {

    String getUserId();

    void checkRole(Role[] roles);

    User getUser();

    boolean isAuth();

    boolean isLoggedIn();

    boolean isAdmin();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

}
