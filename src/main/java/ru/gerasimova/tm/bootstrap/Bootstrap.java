package ru.gerasimova.tm.bootstrap;

import ru.gerasimova.tm.api.repository.ICommandRepository;
import ru.gerasimova.tm.api.repository.IProjectRepository;
import ru.gerasimova.tm.api.repository.ITaskRepository;
import ru.gerasimova.tm.api.repository.IUserRepository;
import ru.gerasimova.tm.api.service.*;
import ru.gerasimova.tm.command.AbstractCommand;
import ru.gerasimova.tm.command.auth.LoginCommand;
import ru.gerasimova.tm.command.auth.LogoutCommand;
import ru.gerasimova.tm.command.auth.RegistrationCommand;
import ru.gerasimova.tm.command.project.*;
import ru.gerasimova.tm.command.system.*;
import ru.gerasimova.tm.command.task.*;
import ru.gerasimova.tm.command.user.*;
import ru.gerasimova.tm.entity.User;
import ru.gerasimova.tm.enumeration.Role;
import ru.gerasimova.tm.exception.empty.EmptyCommandException;
import ru.gerasimova.tm.exception.system.IncorrectArgumentException;
import ru.gerasimova.tm.exception.system.IncorrectCommandException;
import ru.gerasimova.tm.repository.CommandRepository;
import ru.gerasimova.tm.repository.ProjectRepository;
import ru.gerasimova.tm.repository.TaskRepository;
import ru.gerasimova.tm.repository.UserRepository;
import ru.gerasimova.tm.service.*;
import ru.gerasimova.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public final class Bootstrap implements ServiceLocator {

    private static final Class[] COMMANDS = new Class[]{

            HelpCommand.class,
            SystemInfoCommand.class, ExitCommand.class, VersionCommand.class, AboutCommand.class,
            ArgumentsShowCommand.class, CommandsShowCommand.class,

            LoginCommand.class, LogoutCommand.class, RegistrationCommand.class,
            UserInfoCommand.class, UserPasswordUpdateCommand.class,

            TaskListShowCommand.class, TasksClearCommand.class, TaskCreateCommand.class,
            TaskShowByIndexCommand.class, TaskShowByNameCommand.class, TaskShowByIdCommand.class,
            TaskUpdateByIndexCommand.class, TaskUpdateByIdCommand.class, TaskRemoveByNameCommand.class,
            TaskRemoveByIndexCommand.class, TaskRemoveByIdCommand.class,

            ProjectListShowCommand.class, ProjectsClearCommand.class, ProjectCreateCommand.class,
            ProjectShowByIdCommand.class, ProjectShowByIndexCommand.class, ProjectShowByNameCommand.class,
            ProjectUpdateByIdCommand.class, ProjectUpdateByIndexCommand.class, ProjectRemoveByNameCommand.class,
            ProjectRemoveByIndexCommand.class, ProjectRemoveByIdCommand.class,

            UserLockCommand.class, UserUnLockCommand.class, UserRemoveByLoginCommand.class
    };
    private final IUserRepository userRepository = new UserRepository();
    private final IUserService userService = new UserService(userRepository);
    private final IAuthService authService = new AuthService(userService);
    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        for (final Class clazz : COMMANDS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                command.setServiceLocator(Bootstrap.this);
                if (command.name() != null) commands.put(command.name(), command);
                if (command.arg() != null) arguments.put(command.arg(), command);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void initUsersAndData() {
        final User test = userService.create("test", "test", "test@test.ru");
        final User admin = userService.create("admin", "admin", Role.ADMIN);
        taskService.create(test.getId(), "task01", "demo01 by test");
        taskService.create(test.getId(), "task02", "demo02 by test");
        taskService.create(test.getId(), "task03", "demo03 by test");
        projectService.create(test.getId(), "Project01", "demo01 by test");
        projectService.create(test.getId(), "Project02", "demo04 by test");
        projectService.create(test.getId(), "Project03", "demo03 by test");
        taskService.create(admin.getId(), "task04", "demo04 by test");
        taskService.create(admin.getId(), "task05", "demo05 by test");
        taskService.create(admin.getId(), "task06", "demo06 by test");
        projectService.create(admin.getId(), "Project04", "demo04 by test");
        projectService.create(admin.getId(), "Project05", "demo05 by test");
        projectService.create(admin.getId(), "Project06", "demo06 by test");
    }

    public void run(final String[] args) {
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        if (parseArgs(args)) System.exit(0);
        initUsersAndData();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new IncorrectArgumentException(arg);
        argument.execute();
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) throw new EmptyCommandException();
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new IncorrectCommandException(cmd);
        authService.checkRole(command.roles());
        command.execute();

    }

    public IUserService getUserService() {
        return userService;
    }

    public IAuthService getAuthService() {
        return authService;
    }

    public ICommandService getCommandService() {
        return commandService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }
}
